""" hidmsr - a library for interacting with the MSR605X magnetic card reader/writer.

Licensed under the GNU Public License, version 3 or later.

Copyright (c) 2018 Cameron Conn
"""
