
import hidmsr.commands as cmds
import hidmsr.convert as conv

m = cmds.MSRDevice()
m.set_hico()
m.set_bpi(210, 75, 210)

out = m.read_raw()

#print(out)
print(conv.extract_data(out))
